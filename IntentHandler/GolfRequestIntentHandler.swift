//
//  GolfRequestHandler.swift
//  Siri
//
//  Created by Maksym Leshchenko on 8/29/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Foundation

import Foundation
import Intents
import TestCore

class GolfRequestIntentHandler: NSObject, INAddTasksIntentHandling {
    
    public func handle(intent: INAddTasksIntent,
                       completion: @escaping (INAddTasksIntentResponse) -> Swift.Void) {
        
        let taskTitles = intent.taskTitles
        
        guard let phrases = taskTitles else {
            completion(INAddTasksIntentResponse(code: .failure, userActivity: nil))
            return
        }
        
        let resultString = phrases.reduce("") { $0 + $1.spokenPhrase }
        
        var tasks: [INTask] = []
        
        for phrase in phrases {
            let task = INTask(title: phrase, status: .completed, taskType: .completable, spatialEventTrigger: nil, temporalEventTrigger: nil, createdDateComponents: nil, modifiedDateComponents: nil, identifier: nil)
            tasks.append(task)
        }
        
        if !resultString.isEmpty {
            IntentConsumer.saveRecord(GolfRecord(query: resultString))
        }
        
        let response = INAddTasksIntentResponse(code: .success, userActivity: nil)
        //response.modifiedTaskList = intent.targetTaskList
        response.addedTasks = tasks
        print("Intent title - \(String(describing: response.addedTasks?.first?.title))")
        completion(response)
    }
    
    
    func resolveTaskTitles(for intent: INAddTasksIntent, with completion: @escaping ([INSpeakableStringResolutionResult]) -> Void) {
        if let phrases = intent.taskTitles {
            let result = phrases.map({ return INSpeakableStringResolutionResult.success(with: $0) })
            completion(result)
        } else {
            completion([INSpeakableStringResolutionResult.needsValue()])
        }
    }
}
