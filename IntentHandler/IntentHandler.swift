//
//  IntentHandler.swift
//  Siri
//
//  Created by Vadim Osovets on 8/23/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Intents
import TestCore

class IntentHandler: INExtension {
    
    override func handler(for intent: INIntent) -> Any? {
        if intent is INAddTasksIntent {
            return GolfRequestIntentHandler()
        }
        return nil
    }

}

