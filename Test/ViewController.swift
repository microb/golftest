//
//  ViewController.swift
//  GolfPrototype
//
//  Created by Vadim Osovets on 8/23/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import UIKit
import Intents
import TestCore

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var validResultsTableView: UITableView!
    @IBOutlet weak var invalidResultsTableView: UITableView!
    @IBOutlet weak var clearValidResultsButton: UIButton!
    @IBOutlet weak var clearInvalidResultsButton: UIButton!
    
    private var validResults: [String] = []
    private var invalidResults: [String] = []
    private var intentConsumer: IntentConsumer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.validResultsTableView.delegate = self
        self.validResultsTableView.dataSource = self
        self.invalidResultsTableView.delegate = self
        self.invalidResultsTableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidBecomeActive(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        // Test add bla bla
        // Test add lost shot
        // Test add bla
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.intentConsumer = IntentConsumer.load()
        print("RECORDS - \(self.intentConsumer?.records.first?.description ?? "")")
        for record in self.intentConsumer?.records ?? [] {
            print(record.description)
        }
        
        self.populateTables(with: self.intentConsumer?.records ?? [])
    }
    
    // MARK: - Functions
    private func populateTables(with records: [GolfRecord]) {
        self.invalidResults.removeAll()
        self.validResults.removeAll()
        
        for record in records {
            if Verifier.shared.verify(query: record.description) {
                self.validResults.append(record.description)
            } else {
                self.invalidResults.append(record.description)
            }
        }
        
        self.validResultsTableView.reloadData()
        self.invalidResultsTableView.reloadData()
    }
    
    // MARK: - Callbacks
    @IBAction func clearButtonTapped(_ sender: UIButton) {
        switch sender {
        case self.clearValidResultsButton:
            self.validResults.removeAll()
            self.validResultsTableView.reloadData()
            break
        case self.clearInvalidResultsButton:
            self.invalidResults.removeAll()
            self.invalidResultsTableView.reloadData()
            break
        default:
            break
        }
    }
    
    
    // MARK: - UITableView delegate and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case self.validResultsTableView:
            return self.validResults.count
        case self.invalidResultsTableView:
            return self.invalidResults.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell") as? ResultTableViewCell else {
            print("Failed to dequque cell")
            return UITableViewCell() }
        
        switch tableView {
        case self.validResultsTableView:
            cell.configure(with: self.validResults[indexPath.row])
            break
        case self.invalidResultsTableView:
            cell.configure(with: self.invalidResults[indexPath.row])
            break
        default:
            break
        }
        
        return cell
    }
    
    // MARK: - Notifications
    @objc func appDidBecomeActive(_ notification: NSNotification) {
        self.intentConsumer = IntentConsumer.load()
        print("RECORDS - \(self.intentConsumer?.records.first?.description ?? "")")
        for record in self.intentConsumer?.records ?? [] {
            print(record.description)
        }
        
        self.populateTables(with: self.intentConsumer?.records ?? [])
    }
}

