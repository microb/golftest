//
//  Verifier.swift
//  GolfPrototype
//
//  Created by Pavel Borisov on 8/30/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Foundation

class Verifier {
    static let shared = Verifier()
    
    init() {}
    
    // MARK: - Verify params
    let shots: [String] = ["Tee Box", "Lost Shot", "Fairway", "Green"]
    let clubs: [String] = ["Driver", "5 Iron", "Putter"]
    let distances: [String] = ["Yards", "Feet", "Yard"]
    
   /* Hey Siri- <Golf>- Tee Box, Driver, 456 Yards
    Hey Siri- <Golf>- Lost Shot (Record as Null, In Club and Distance)
    Hey Siri- <Golf>- Fairway, 5 Iron, 176 Yards
    Hey Siri- <Golf>, Green, Putter, 20 Feet
    Hey Siri- <Golf>, Green, Putter, 2 Feet */
    
    // MARK: - Functions
    func verify(query: String) -> Bool {
        var doesContainShot = false
        var doesContainClub = false
        var doesContainDistance = false
        
        for shot in self.shots {
            if query.lowercased().contains(shot.lowercased()) {
                if shot == "Lost Shot" {
                    // Lost shot comes without params by default
                    return true
                }
                
                doesContainShot = true
            }
        }
        
        for club in self.clubs {
            if query.lowercased().contains(club.lowercased()) {
                doesContainClub = true
            }
        }
        
        for distance in distances {
            if query.lowercased().contains(distance.lowercased()) {
                doesContainDistance = true
            }
        }
        
        if doesContainShot, doesContainClub, doesContainDistance {
            return true
        } else {
            return false
        }
    }
}
