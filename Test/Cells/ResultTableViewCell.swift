//
//  ResultTableViewCell.swift
//  GolfPrototype
//
//  Created by Pavel Borisov on 8/28/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with title: String) {
        self.titleLabel.text = title
    }
}
