//
//  IntentConsumer.swift
//  GolfPrototypeCore
//
//  Created by Pavel Borisov on 8/29/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Foundation

public protocol IntentConsumerPorotocol {
    func show(records: [GolfRecord]?)
}

public class IntentConsumer {
    
    private static var userDefaults: UserDefaults = {
        guard let defaults = UserDefaults(suiteName: "group.golfPrototype") else {
            fatalError("failed to create UserDefaults suite")
        }
        
        return defaults
    }()
    
    private static let decoder = JSONDecoder()
    private static let encoder = JSONEncoder()
    
    private var delegate: IntentConsumerPorotocol?
    
    public var records: [GolfRecord]
    
    public init(_ records: [GolfRecord]) {
        self.records = records
    }
    
    public static func getRecords() -> [GolfRecord]? {
        
        if let records = userDefaults.value(forKey: Constants.Keys.golfRecord) as? Data {
            
            if let recordsDecoded = try? decoder.decode(Array.self, from: records) as [GolfRecord] {
                return recordsDecoded
            } else {
                return nil
            }
            
        } else {
            return nil
        }
        
    }
    
    public static func load() -> IntentConsumer {
        var records = [GolfRecord]()
        
        if let fetchedRecords = self.getRecords() {
            records.append(contentsOf: fetchedRecords)
        }
        
        return IntentConsumer(records)
    }
    
    public static func saveRecord(_ record: GolfRecord) {
        var records: [GolfRecord] = [GolfRecord]()
        
        if let savedRecords = IntentConsumer.getRecords() {
            records.append(contentsOf: savedRecords)
        }
        
        records.append(record)
        
        if let encodedRecord = try? IntentConsumer.encoder.encode(records) {
            IntentConsumer.userDefaults.set(encodedRecord, forKey: Constants.Keys.golfRecord)
        }
    }
    
}
