//
//  Constants.swift
//  GolfPrototypeCore
//
//  Created by Maksym Leshchenko on 8/30/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Keys {
        static let golfRecord = "GolfRecord"
    }
    
}
