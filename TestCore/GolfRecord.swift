//
//  GolfRecord.swift
//  GolfPrototypeCore
//
//  Created by Maksym Leshchenko on 8/30/18.
//  Copyright © 2018 Micro-b. All rights reserved.
//

import Foundation

public struct GolfRecord: Codable {
    public let description: String
    
    enum Keys: String, CodingKey {
        case description
    }
    
    public init(query: String) {
        self.description = query
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        description = try values.decode(String.self, forKey: .description)
    }
}
